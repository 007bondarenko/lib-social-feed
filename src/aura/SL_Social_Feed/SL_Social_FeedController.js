({
	doInit: function (component, event, helper) {
		var account = component.get('v.account');
		var height = component.get('v.height');
		var width = component.get('v.width');
		var url = ''.concat(
			component.get('v.url'),
			'?account=',
			account || account.length > 0 ? account : 'silverlinecrm',
			'&height=',
			height,
			'&width=',
			width
		);
		component.set(
			'v.url',
			url
		);
	}
})