({
	createCallback: function (cmp, newCmp, status, errorMessage) {
		// console.log(status);
		if (status === 'SUCCESS') {
			var body = cmp.get('v.body');
			body.push(newCmp);
			cmp.set('v.body', body);
		}
		if (status === 'ERROR') {
			console.error(errorMessage)
		}

	},
	socialChoose: function (social, component) {
		var url = this.getUrlPromise(component);
		url.then(
				$A.getCallback(function (resp) {
					switch (social) {
						case 'Facebook':
							var url = ''.concat(resp, '/apex/SL_FacebookPage');
							this.createCMP(component, 'c:SL_Social_Feed', url);
							break;
						case 'Twitter':
							var url = ''.concat(resp, '/apex/SL_TwitterFeed');
							this.createCMP(component, 'c:SL_Social_Feed', url);
						default:
							console.log('default');
							break;
					}
				}).bind(this)
			)
			.catch($A.getCallback(function (err) {
				console.error(err);
			}));
	},
	createCMP: function (component, name, url) {
		var width = component.get('v.width');
		var account = component.get('v.account');
		var height = component.get('v.height');
		var createCallback = this.createCallback.bind(null, component);
		var attributes = {
			url: url,
			account: account,
			height: height,
			width: width
		};
		var args = [name, attributes, createCallback]
		$A.createComponent.apply(this, args);
	},
	getUrlPromise: function (component) {
		return new Promise(this.getUrlCallback.bind(null, component));
	},
	getUrlCallback: function (component, resolve, reject) {
		var action = component.get('c.sfUrl');
		action.setCallback(
			this,
			$A.getCallback(function (response) {
				var state = response.getState();
				if (state === 'SUCCESS') {
					return resolve(response.getReturnValue());

				} else {
					return reject(response.getError());
				}
			})
		);
		$A.enqueueAction(action);
	}
})