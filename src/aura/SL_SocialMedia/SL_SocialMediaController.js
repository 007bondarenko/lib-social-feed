({
  doInit: function(component, event, helper) {
    var social = component.get('v.socialMedia');
    helper.socialChoose(social, component);
  }
});
