public class SL_Social_Feed {
    public Map<String,String> params {
        get {
            return ApexPages.currentPage().getParameters();
        }
        private set;
    }

    @AuraEnabled
    public static String sfUrl() {
        return URL.getSalesforceBaseUrl().toExternalForm();
    }
}