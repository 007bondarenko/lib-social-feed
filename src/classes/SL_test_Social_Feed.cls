@isTest
public class SL_test_Social_Feed {
	@isTest
    static void testSocialFeed() {
        Test.setCurrentPageReference(new PageReference('Page.SL_FacebookPage'));
        System.currentPageReference().getParameters().put('id', '12345');
        
        SL_Social_Feed socialPage = new SL_Social_Feed();
		System.assertEquals(socialPage.params.get('id'), ApexPages.currentPage().getParameters().get('id'));
        System.assertEquals(URL.getSalesforceBaseUrl().toExternalForm(), SL_Social_Feed.sfUrl());
    }
}